package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"regexp"
	"sort"
	"strings"
	"syscall"

	"github.com/gofiber/fiber/v2"
	"gonum.org/v1/gonum/stat/combin"
)

const (
	UKEIndividualsCSV = "https://amator.uke.gov.pl/pl/individuals/export.csv"
	UKEClubsCSV       = "https://amator.uke.gov.pl/pl/clubs/export.csv"
)

var (
	CallsignRegexp = regexp.MustCompile("(SP|SQ|HF|SO|SN|3Z)[0-9][A-Z0-9]{1,5}")
	GlobalCache    = map[string]Callsign{}
)

type Char struct {
	LetterICAO    string
	LetterPolish  string
	LetterCW      string
	LetterRussian string
	LetterCyrylic string

	SyllableICAO    []string
	SyllablePolish  []string
	SyllableRussian []string
}

type Callsign struct {
	Callsign string

	Prefix string
	Region string
	Suffix string

	LengthPolish  float32
	LengthICAO    float32
	LengthCW      float32
	LengthRussian float32

	ScoreICAO    float32
	ScorePolish  float32
	ScoreRussian float32
	ScoreCW      float32
	ScoreTotal   float32

	LettersICAO    string
	LettersPolish  string
	LettersRussian string
	LettersCyrylic string
	LettersCW      string

	SyllableICAO    string
	SyllablePolish  string
	SyllableRussian string

	SyllablePolishCount  float32
	SyllableICAOCount    float32
	SyllableRussianCount float32

	Allocated               bool
	AllocatedExpirationDate string
	AllocatedCity           string
	AllocatedPower          string
	AllocatedCategory       string
}

type FindCallsignsConfig struct {
	Regions         map[string]bool
	Prefixes        map[string]bool
	MaxSuffixLength int
	MinSuffixLength int
	Limit           int
	ShowAllocated   bool
}

func DisassembleCallsign(cs string) (c Callsign) {
	if _, ok := GlobalCache[cs]; ok {
		return GlobalCache[cs]
	}

	const Scores = 4

	c.Callsign = cs
	c.Prefix = cs[:2]
	c.Region = string(cs[2])
	c.Suffix = cs[3:]

	for _, ch := range cs {
		c.LettersICAO += CharacterMap[ch].LetterICAO + " "
		c.LettersPolish += CharacterMap[ch].LetterPolish + " "
		c.LettersRussian += CharacterMap[ch].LetterRussian + " "
		c.LettersCyrylic += CharacterMap[ch].LetterCyrylic + " "
		c.LettersCW += CharacterMap[ch].LetterCW + " "
		c.SyllableICAOCount += float32(len(CharacterMap[ch].SyllableICAO))
		c.SyllablePolishCount += float32(len(CharacterMap[ch].SyllablePolish))
		c.SyllableRussianCount += float32(len(CharacterMap[ch].SyllableRussian))
		c.SyllableICAO += strings.Join(CharacterMap[ch].SyllableICAO, "-") + " "
		c.SyllablePolish += strings.Join(CharacterMap[ch].SyllablePolish, "-") + " "
		c.SyllableRussian += strings.Join(CharacterMap[ch].SyllableRussian, "-") + " "
	}

	for _, let := range []*string{
		&c.LettersICAO, &c.LettersPolish, &c.LettersRussian, &c.LettersCW, &c.LettersCyrylic,
		&c.SyllableICAO, &c.SyllablePolish, &c.SyllableRussian,
	} {
		*let = strings.TrimSuffix(*let, " ")
	}

	c.LengthPolish = float32(len(strings.ReplaceAll(c.LettersPolish, " ", "")))
	c.LengthICAO = float32(len(strings.ReplaceAll(c.LettersICAO, " ", "")))
	c.LengthRussian = float32(len(strings.ReplaceAll(c.LettersRussian, " ", "")))
	c.LengthCW = float32(len(strings.ReplaceAll(c.LettersCW, " ", "")))

	c.ScoreCW = c.LengthCW
	c.ScoreICAO = (c.LengthICAO * c.SyllableICAOCount)
	c.ScorePolish = (c.LengthPolish * c.SyllablePolishCount)
	c.ScoreRussian = (c.LengthRussian * c.SyllableRussianCount)
	c.ScoreTotal = (c.ScoreCW + c.ScorePolish + c.ScoreICAO + c.ScoreRussian) / Scores

	GlobalCache[cs] = c

	return c
}

func ValidateCallsign(cs string) bool {
	return CallsignRegexp.MatchString(cs)
}

func UpdateAllocatedCallsigns() (err error) {
	log.Println("Fetching currently used callsigns from UKE")

	var HTTPClient http.Client

	for _, u := range []string{UKEClubsCSV, UKEIndividualsCSV} {
		addr, err := url.Parse(u)
		if err != nil {
			return fmt.Errorf("error parsing URL %s: %w", addr, err)
		}

		resp, err := HTTPClient.Do(
			&http.Request{
				Method: "GET",
				URL:    addr,
			})
		if err != nil {
			return fmt.Errorf("error fetching CSV from url %s: %w", resp.Request.URL, err)
		}
		defer resp.Body.Close()
		reader := csv.NewReader(resp.Body)
		reader.Comma = ';'

		data, err := reader.ReadAll()
		if err != nil {
			return fmt.Errorf("error parsing CSV from url %s: %w", resp.Request.URL, err)
		}

		for _, entry := range data[1:] {
			cs := DisassembleCallsign(entry[2])
			cs.Allocated = true
			cs.AllocatedExpirationDate = entry[1]
			cs.AllocatedCategory = entry[3]
			cs.AllocatedPower = entry[4]
			cs.AllocatedCity = entry[5]
			GlobalCache[entry[2]] = cs
		}
	}

	return nil
}

func generateSuffixTable(min, max int) (suffixList []string) {
	const (
		AlphabetCount = 26
		ASCIIOffset   = 65
	)

	for i := min; i <= max; i++ {
		log.Printf("Generating %d char suffixes...\n", i)
		p := combin.Permutations(AlphabetCount, i)

		for _, c := range p {
			var s []string
			for _, v := range c {
				s = append(s, string(byte(v+ASCIIOffset)))
			}

			suffixList = append(suffixList, strings.Join(s, ""))
		}
	}

	return suffixList
}

func FindCallsigns(config FindCallsignsConfig) (callsigns []Callsign) {
	var (
		LeftSides  []string
		RightSides []string
		Candidates []string
	)

	for p := range config.Prefixes {
		for r := range config.Regions {
			LeftSides = append(LeftSides, p+r)
		}
	}

	RightSides = generateSuffixTable(config.MinSuffixLength, config.MaxSuffixLength)

	for _, l := range LeftSides {
		for _, r := range RightSides {
			Candidates = append(Candidates, l+r)
		}
	}

	for _, cand := range Candidates {
		if candcs, ok := GlobalCache[cand]; ok {
			if !config.ShowAllocated {
				if candcs.Allocated {
					continue
				}
			}
		}

		callsigns = append(callsigns, DisassembleCallsign(cand))
	}

	sort.Slice(callsigns, func(i, j int) bool {
		return callsigns[i].ScoreTotal < callsigns[j].ScoreTotal
	})

	if len(callsigns) < config.Limit {
		return callsigns
	}

	return callsigns[:config.Limit]
}

func RestoreGlobalCache() error {
	if _, err := os.Stat("data/cache.json"); os.IsNotExist(err) {
		log.Println("Global cache not found")

		return nil
	}

	log.Println("Restoring from global cache...")

	file, err := ioutil.ReadFile("data/cache.json")
	if err != nil {
		return fmt.Errorf("failed reading JSON global cache data: %w", err)
	}

	err = json.Unmarshal(file, &GlobalCache)
	if err != nil {
		return fmt.Errorf("failed parsing JSON data from global cache: %w", err)
	}

	return nil
}

func DumpGlobalCache() error {
	file, err := json.MarshalIndent(GlobalCache, "", "  ")
	if err != nil {
		return fmt.Errorf("failed marshalling JSON data from global cache: %w", err)
	}

	err = ioutil.WriteFile("data/cache.json", file, 0600)
	if err != nil {
		return fmt.Errorf("failed writing global cache to file: %w", err)
	}

	return nil
}

func main() {
	RestoreGlobalCache()

	err := UpdateAllocatedCallsigns()
	if err != nil {
		log.Println("error getting allocated callsigns: ", err)
	}

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-c
		log.Println("Received interrupt, dumping data to cache...")
		DumpGlobalCache()
		os.Exit(1)
	}()
	DumpGlobalCache()

	app := fiber.New()
	app.Get("/api/cache", func(c *fiber.Ctx) error {
		return c.JSON(GlobalCache)
	})
	app.Get("/api/call/:call", func(c *fiber.Ctx) error {
		call := c.Params("call")
		if ValidateCallsign(call) {
			return c.JSON(DisassembleCallsign(c.Params("call")))
		}
		return c.Status(400).SendString("invalid callsign")
	})
	app.Get("/api/search", func(c *fiber.Ctx) error {
		var config FindCallsignsConfig
		c.BodyParser(&config)

		calls := FindCallsigns(config)

		return c.JSON(calls)
	})
	log.Fatal(app.Listen(":3000"))
}
