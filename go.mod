module gitlab.com/skrzyp/callsign-api

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.15.0
	gonum.org/v1/gonum v0.9.3
)
